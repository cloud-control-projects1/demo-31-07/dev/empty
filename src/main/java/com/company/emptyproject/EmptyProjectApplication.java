package com.company.emptyproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class EmptyProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmptyProjectApplication.class, args);
    }
}
