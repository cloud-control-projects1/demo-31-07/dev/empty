import { TranslationMessages } from "ra-core";
import englishMessages from "ra-language-english";
import { mergeMessages } from "./mergeMessages";

const messages: TranslationMessages = {
  ...englishMessages,

  resources: {
    Company: {
      name: "Company |||| Companies",

      fields: {
        date: "Date",
        name: "Name",
        number: "Number",
        type: "Type"
      }
    }
  },

  enums: {
    Type: {
      PRIVATE: "Private",
      PUBLIC: "Public"
    }
  }
};

export const en = mergeMessages(
  messages,
  [] // place addon messages here
);
